
/*
## Задание

Нарисовать на странице круг используя параметры, которые введет пользователь.

#### Технические требования:
- При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
- При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
- У вас может возникнуть желание поставить обработчик события на каждый круг для его исчезновения. Это неэффективно, так делать не нужно. На всю страницу должен быть только один обработчик событий, который будет это делать.
*/



const btn = document.querySelector('button');
const body = document.querySelector('body');

let diametr=0;

btn.addEventListener('click', function() {
    btn.style.display = 'none';
    diametr = parseInt(prompt('Введите диаметр круга')); 
    let container = document.createElement('div');
    container.className = 'container';
    container.style.width = `${diametr * 10 + 100}px`; 
    document.body.append(container);
    let cir;
    for(let i = 0; i<100; i++){
        cir = new Circle(diametr);
        cir.drow(container);
    }
})

body.addEventListener('click', function(event) {
    if(event.target.classList.contains('circle') && document.querySelectorAll('.circle').length > 1) {
        event.target.remove();
    }  else if( event.target.classList.contains('circle') && document.querySelectorAll('.circle').length === 1){
        event.target.remove();
        document.querySelector('.container').remove();
        btn.style.display = 'block';
    }
})

class Circle {
    constructor(diametr) {
        this.diametr = diametr;
    }

    getColor() {
        return `rgb(${Math.floor( Math.random()*255)}, ${Math.floor( Math.random()*255)}, ${Math.floor( Math.random()*255)})`;
    }

    drow(place) {
        let c = document.createElement('div');
        c.className = 'circle';
        c.style.width = `${this.diametr}px`;
        c.style.height = `${this.diametr}px`;
        c.style.borderRadius = '100%';
        c.style.backgroundColor = this.getColor();
        c.style.margin = '5px';
        c.style.cursor = 'pointer';
        c.style.display = 'block';
        c.style.float = 'left';
        place.append(c);
    }
}
